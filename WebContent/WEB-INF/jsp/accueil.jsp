<link rel="stylesheet" type="text/css" href="css/header.css">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Epicerie</title>
</head>
<body style= "background-color: orange;">
	<%@ include file="../jspf/header.jspf"%>
	
	<h1 style="text-align: center; margin : auto; color: purple; font-size: 25px;">Cher utilisateur, veuillez cliquer sur le bouton qui vous correspond</h1>
	
	<br><br>

	<button type="button" style="background-color: green; text-align: center; width: 300px; height: 50px; display: block; margin-left : auto; margin-right : auto;">
		<a style = "font-size: 30px; color: white;" href="interfaceProducteur.jsp">Producteur</a>
	</button>
	<br>
	<br>

	<button style="background-color: tomato; text-align: center; width: 300px; height: 50px; display: block; margin-left : auto; margin-right : auto;" type="button">
		<a style = "font-size: 30px; color: white;" href="interfaceVendeur.jsp">Vendeur</a>
	</button>

	<br>
	<br>

	<button style="background-color: blue; text-align: center; width: 300px; height: 50px; display: block; margin-left : auto; margin-right : auto;" type="button">
		<a style = "font-size: 30px; color: white;" href="boutique.jsp">Acheteur</a>
	</button>



</body>
</html>