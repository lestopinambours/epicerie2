<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Boutique</title>
</head>
<body style="background-color: orange;">
	<%@ include file="../jspf/header.jspf"%>
	<h1>Nos produits disponibles</h1>
	<table BORDER="1">

		<TR>
			<TH>Nom du produit</TH>
			<TH>Description</TH>
			<TH>Ville</TH>
			<TH>Region</TH>
			<TH>Prix unitaire</TH>
			<TH>Quantité disponible</TH>
			<TH>Quantité à commander</TH>
			<TH>Panier</TH>
		</TR>
		<c:forEach var="current" items="${ produit }">
			<TR>
				<TD align="center">${ current.nom }</TD>
				<TD align="center">${ current.description }</TD>
				<TD align="center">${ current.ville_produit }</TD>
				<TD align="center">${ current.region }</TD>
				<TD align="center">${ current.prix_unit }</TD>
				<TD align="center">${ current.quantite }</TD>
				<TD align="center"><input type="number" placeholder="Saisir quantité"></TD>
				<TD align="center"><input type="button" value="Ajouter au panier"><TD />
			</TR>
		</c:forEach>
	</table>
</body>
</html>