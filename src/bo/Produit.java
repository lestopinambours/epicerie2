package bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="produits")
public class Produit {
	
@Id @GeneratedValue(strategy=GenerationType.IDENTITY)	
	@Column(name="id")
	private int id;	
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="description")
	private String description;
	
	@Column(name="region")
	private String region;
	
	@Column(name="prix_unit")
	private int prix_unit;
	
	@Column(name="ville_produit")
	private String ville_produit;
	
	@Column(name="quantite")
	private int quantite;
	
	@Column(name="id_utilisateurs")
	private int id_utilisateurs;
	
	public Produit() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public int getPrix_unit() {
		return prix_unit;
	}

	public void setPrix_unit(int prix_unit) {
		this.prix_unit = prix_unit;
	}

	public String getVille_produit() {
		return ville_produit;
	}

	public void setVille_produit(String ville_produit) {
		this.ville_produit = ville_produit;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public int getId_utilisateurs() {
		return id_utilisateurs;
	}

	public void setId_utilisateurs(int id_utilisateurs) {
		this.id_utilisateurs = id_utilisateurs;		
	}
}