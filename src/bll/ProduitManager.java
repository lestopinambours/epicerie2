package bll;

import java.util.List;

import bo.Produit;
import dal.ProduitDAO;

public class ProduitManager {
	
private ProduitDAO produitDao;
	
	public ProduitDAO getProduitDao() {
		return produitDao;
	}

	public void setProduitDao(ProduitDAO produitDao) {
		this.produitDao = produitDao;
	}
	
	//Retrouver tous les produits
	public List<Produit> findAll() {
		return produitDao.findAll();
	}
	
	//Retrouver un produit avec son index
	//Ajouter
	//Supprimer
	//Modifier
	
}
