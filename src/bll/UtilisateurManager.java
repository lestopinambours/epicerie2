package bll;

import java.util.List;

import bo.Utilisateur;
import dal.UtilisateurDAO;

public class UtilisateurManager {

	private UtilisateurDAO dao;
	
	
	public UtilisateurDAO getDao() {
		return dao;
	}
	
	public void setDao(UtilisateurDAO dao) {
		this.dao = dao;
	}

	//A QUOI CORRESPOND WEAKISVALID ?
//	public int utilisateurValide(Utilisateur utilisateur) {
//		//return dao.isValid(utilisateur);
//		return dao.weakIsValid(utilisateur);
//	}
	
	
	public List<Utilisateur> findAll() {
		return dao.findAll();
	}
	
	public void ajouterUtilisateur(String nom, String prenom, String pseudo, 
									String password, String adresse, String ville, 
									String region, int statut, int solde) {
		
		Utilisateur tmp = new Utilisateur();
		
		tmp.setNom(nom);
		tmp.setPrenom(prenom);
		tmp.setPseudo(pseudo);
		tmp.setPassword(password);
		tmp.setAdresse(adresse);
		tmp.setVille(ville);
		tmp.setRegion(region);
		tmp.setStatut(statut);
		tmp.setSolde(solde);
		
		dao.insert(tmp);
	}
	
	public Utilisateur selectById(int idUtilisateur) {
		return dao.selectById(idUtilisateur);
	}
	
	public void modifierUtilisateur(int id, String nom, String prenom, String pseudo, 
			String password, String adresse, String ville, 
			String region, int statut, int solde) {
		
		
		
		Utilisateur tmp = new Utilisateur();
		
		//SETTER SUR UN ID ?
		tmp.setId(id);
		tmp.setNom(nom);
		tmp.setPrenom(prenom);
		tmp.setPseudo(pseudo);
		tmp.setPassword(password);
		tmp.setAdresse(adresse);
		tmp.setVille(ville);
		tmp.setRegion(region);
		tmp.setStatut(statut);
		tmp.setSolde(solde);
		
		
		dao.update(tmp);
		
	}
	
	public void supprimerUtilisateur(int id) {
		dao.delete(id);
	}
	

	



	
}

