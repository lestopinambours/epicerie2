package dal;

import java.util.List;

import bo.Utilisateur;

public interface UtilisateurDAO {

	List<Utilisateur> findAll();
	
	void insert(Utilisateur utilisateur);
	
	Utilisateur selectById(int id);
	
	void update(Utilisateur utilisateur);
	
	void delete(int id) ;
	
	
}
