package dal;

import bo.Produit;

import java.util.List;

public interface ProduitDAO {

	List<Produit> findAll();
}
