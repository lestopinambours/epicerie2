package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bo.Produit;

public class ProduitDAOJdbcImpl implements ProduitDAO {

	
	private static final String SELECT_ALL =
			"SELECT p.nom, p.description, p.region, p.prix_unitaire, p.quantite, p.ville_produit"
			+ " FROM Produits p";

	@Override
	public List<Produit> findAll() {
		List<Produit> Produits = new ArrayList<Produit>();
		
		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement ps = cnx.prepareStatement(SELECT_ALL);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				Produits.add(rsToProduit(rs));
			}
			
			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return Produits;
	}

	private Produit rsToProduit(ResultSet rs) throws SQLException {
		Produit produit = new Produit();
		
		//produit.setId(rs.getInt("id_Produit"));
		produit.setNom(rs.getString("nom"));
		produit.setDescription(rs.getString("description"));
		produit.setRegion(rs.getString("region"));
		produit.setPrix_unit(rs.getInt("prix_unitaire"));
		produit.setQuantite(rs.getInt("quantite"));
		produit.setVille_produit(rs.getString("ville_produit"));

		return produit;
	}

}

