package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import bll.ProduitManager;
import bo.Produit;

/**
 * Servlet implementation class ServletBoutique
 */
@WebServlet("/boutique")
public class ServletBoutique extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ServletBoutique() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ApplicationContext context =new ClassPathXmlApplicationContext("springContext.xml");
		ProduitManager pm = context.getBean("pm", ProduitManager.class);
		List<Produit> produit = pm.findAll();
		request.setAttribute("produit", produit);
		this.getServletContext().getNamedDispatcher("boutique").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}